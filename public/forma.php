<!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="./style.css">
        <title>Peryatinskaya Evgeniya 27/1</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <style>
        .error {
        border: 2px solid red;
        }
    </style>
       </head>
        <body>
            <header>
    <h1>Форма:</h1>
    </header>
    <?php
    if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
    print($message);
    }
    print('</div>');
    }
    ?>
    <div class="my-blocks">
        <div id="form">
    <form name="forma1" action=" " enctype="multipart/form-data" method="post">
    <fieldset>
    <p><label>Имя<em></em></label><input type="text" size = "32" name="name" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>" ></p>
    <p><label>E-mail</label><input type="email" size = "32" <?php if ($errors['email']) {print 'class="error"';} ?> value = "<?php print $values['email']; ?>" name = "email"></p>
    <p><label>Дата рождения</label><input name="date" type = "date" <?php if ($errors['date']) {print 'class="error"';} ?> value = "<?php print $values['date']; ?>"></p>
    <h2>Пол:</h2>
    <p><input type = "radio" name = "dzen1" value = "M" <?php if ($errors['gender']) {print 'class="error"';} ?> <?php if ('M' === $values['gender']){print 'checked="checked"';} ?>>Мужской</p>
    <p><input type = "radio" name = "dzen1" value = "F" <?php if ($errors['gender']) {print 'class="error"';} ?> <?php if ('F' === $values['gender']){print 'checked="checked"';} ?> >Женский</p>
    <h2>Количество конечностей:</h2>
    <label><input type = "radio" name = "dzen2" value = "1"  <?php if ($errors['dzen2']) {print 'class="error"';} ?> <?php if ('1' === $values['dzen2']){print 'checked="checked"';}  ?> >1</label>
    <label><input type = "radio" name = "dzen2" value = "2"  <?php if ($errors['dzen2']) {print 'class="error"';} ?> <?php if ('2' === $values['dzen2']){print 'checked="checked"';}  ?> >2</label>
    <label><input type = "radio" name = "dzen2" value = "3"  <?php if ($errors['dzen2']) {print 'class="error"';} ?> <?php if ('3' === $values['dzen2']){print 'checked="checked"';}  ?>>3</label>
    <label><input type = "radio" name = "dzen2" value = "4" <?php if ($errors['dzen2']) {print 'class="error"';} ?> <?php if ('4' === $values['dzen2']){print 'checked="checked"';}  ?>>4</label>
    <label><input type = "radio" name = "dzen2" value = "more"  <?php if ($errors['dzen2']) {print 'class="error"';} ?> <?php if ('more' === $values['dzen2']){print 'checked="checked"';}  ?>>больше</label>

    <h2><label>Сверхспособности:</label></h2>
    <select name="strenght[]" multiple=multiple>
    <option>Выберите:</option>
    <option value="1" <?php if ($errors['strenght']) {print 'class="error"';} ?>
            <?php
              $arr = str_split($values['strenght']);
              foreach($arr as $el)
                if ($el == 1)
                  print 'selected';
            ?>
              >Бессмертие</option>
              <option value="2" <?php if ($errors['strenght']) {print 'class="error"';} ?>
              <?php
              $arr = str_split($values['strenght']);
              foreach($arr as $el)
                if ($el == 2)
                  print 'selected';
            ?>
              > Прохождение сквозь стены</option>
              <option value="3" <?php if ($errors['strenght']) {print 'class="error"';} ?>
              <?php
              $arr = str_split($values['strenght']);
              foreach($arr as $el)
                if ($el == "3")
                  print 'selected';
            ?>
              >Левитация</option>
              <option value="4" <?php if ($errors['strenght']) {print 'class="error"';} ?>
              <?php
              $arr = str_split($values['strenght']);
              foreach($arr as $el)
                if ($el == "4")
                  print 'selected';
            ?>
              >Web</option>
    </select>
    <p><label>Расскажите о себе...</label></p>
    <p> <textarea name="biography" maxlength="200" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php print $values['biography']; ?></textarea></p>

 		<p><input name="check1"  type = "checkbox"   value = "yes" <?php if ($errors['check1']) {print 'class="error"';} ?> <?php if ('yes'===$values['check1']) {print 'checked="checked"';} ?>>Ознакомлен(-а) с правилами пользования</p>
                <p><input name="confirm"  type = "submit"  value = "Отправить"></p>

    </fieldset>
    </form>
    </div>
    </div>
    </body>
    </html>
